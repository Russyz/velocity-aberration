import os
from astropy.units.cgs import C
from erfa.core import DD2R, DR2AS
import pandas as pd
import numpy as np
import erfa
import math
from erfa import DAYSEC, DAS2R, DR2D, CMPS
from astropy import coordinates as coord
from astropy import units as u
from astropy.time import Time
    
def cal_ele(dxyzt):
    dxt = dxyzt[0]
    dyt = dxyzt[1]
    dzt = dxyzt[2]
    xsta = np.array([-2358691.210,5410611.484,2410087.607])
    LAMBDA,PHI,_ = erfa.gc2gd(1,xsta)
    sp3R   = np.linalg.norm([dxt,dyt,dzt])
    gvs = np.zeros(3)
    gvs[0] =  math.cos(PHI)* math.cos(LAMBDA)			# Station X unit vector
    gvs[1] =  math.cos(PHI)* math.sin(LAMBDA)			# Station Y unit vector
    gvs[2] =  math.sin(PHI)					# Station Z unit vector
    dstn   =  np.linalg.norm(gvs)	# Normalise the unit vectors
    czd    =  (dxt*gvs[0]+dyt*gvs[1]+dzt*gvs[2])/(sp3R*dstn)		# Zenith height component of SAT->STAT vector / vector range
    altc   =  math.asin(czd)*360.0/(2.0*np.pi)
    return altc

# calculate the azimuth of retro-reflectors
def cal_azi(dxyzt):
    dxt = dxyzt[0]
    dyt = dxyzt[1]
    dzt = dxyzt[2]
    xsta = np.array([-2358691.210,5410611.484,2410087.607])
    LAMBDA,PHI,H = erfa.gc2gd(1,xsta)
    gvx = np.zeros(3)
    gvx[0] = -math.sin(PHI)*math.cos(LAMBDA)
    gvx[1] = -math.sin(PHI)*math.sin(LAMBDA)
    gvx[2] = math.cos(PHI)

    gvy =  np.zeros(3)
    gvy[0] = -math.sin(LAMBDA)
    gvy[1] = math.cos(LAMBDA)
    gvy[2] = 0

    cx = (dxt*gvx[0]+dyt*gvx[1]+dzt*gvx[2])
    cy = (dxt*gvy[0]+dyt*gvy[1]+dzt*gvy[2])
    azic = math.atan2(cy, cx) * DR2D
    if azic < 0:
        azic += 360
    return azic
def length(vector):    
    sum = 0
    for i in (vector):
        sum = sum + i**2
    return np.sqrt(sum)

def aberration(v, theta, phi):   
    alpha = 2*length(np.array(v))/CMPS   

    theta_r = np.arccos(((1-alpha**2)*math.cos(theta))/(1+alpha**2+2*alpha*math.sin(theta)*math.cos(phi)))
    #phi_r = np.arctan((1-alpha**2)*math.sin(theta)*math.cos(phi)/(alpha+math.sin(theta)*math.cos(phi)))
    phi_r = np.arctan((1-alpha**2)*math.sin(theta)*math.sin(phi)/(2*alpha+(1+alpha**2)*math.sin(theta)*math.cos(phi)))
    if phi_r < 0:
        phi_r+=2*np.pi
    #phi_r = np.arctan(CMPS*math.sin(phi)*np.sin(theta)-2*length(np.array(v))/(CMPS*np.cos(theta))/np.sin(np.arctan((CMPS*math.sin(phi)*np.sin(theta)-2*length(np.array(v)))/CMPS*math.cos(phi)*math.sin(theta))))
    #theta_R = np.arctan((CMPS*np.sin(phi)*np.sin(theta)-2*length(np.array(v)))/(CMPS*math.cos(phi)*math.sin(theta)))
    delta_theta = (theta_r - theta)
    delta_phi = ((phi_r) - (phi))
    if delta_phi > np.pi/2:
        delta_phi -= np.pi
    elif delta_phi < -np.pi/2:
        delta_phi += np.pi
    return delta_theta * DR2AS, delta_phi * DR2AS
from skyfield.constants import ERAD
from skyfield.functions import angle_between, length_of
from skyfield.searchlib import find_maxima

def f(t):
    e = earth.at(t).position.au
    s = sun.at(t).position.au
    m = moon.at(t).position.au
    return angle_between(s - e, m - e)

e = earth.at(t).position.m
m = moon.at(t).position.m
s = sun.at(t).position.m

solar_radius_m = 696340e3
moon_radius_m = 1.7371e6

pi_m = np.arcsin(ERAD / length_of(m - e))
pi_s = np.arcsin(ERAD / length_of(s - e))
s_s = np.arcsin(solar_radius_m / length_of(s - e))

pi_1 = 0.998340 * pi_m

sigma = angle_between(s - e, e - m)
s_m = np.arcsin(moon_radius_m / length_of(e - m))

penumbral = sigma < 1.02 * (pi_1 + pi_s + s_s) + s_m
partial = sigma < 1.02 * (pi_1 + pi_s - s_s) + s_m
total = sigma < 1.02 * (pi_1 + pi_s - s_s) - s_m
t, y = find_maxima(start_time, end_time, f)
mask = penumbral | partial | total

t = t[mask]
penumbral = penumbral[mask]
partial = partial[mask]
total = total[mask]

file = r'E:\wechat\WeChat Files\zcs1103\FileStorage\File\2021-11\DRO.txt'
array = np.genfromtxt(file)
headers = ['Year', 'Month', 'Day', 'Hour', 'Minute', 'Second', 'px_j2000', 'py_j2000', 'pz_j2000','vx_j2000', 'vy_j2000', 'vz_j2000' ]
pre_orbit = pd.DataFrame(data=array, index=array[:, 1], columns=headers)
year = np.array(pre_orbit['Year'])
month = np.array(pre_orbit['Month'])
day = np.array(pre_orbit['Day'])
hour = np.array(pre_orbit['Hour'])
minute = np.array(pre_orbit['Minute'])
second = np.array(pre_orbit['Second'])
px_j2000 = np.array(pre_orbit['px_j2000'])
py_j2000 = np.array(pre_orbit['py_j2000'])
pz_j2000 = np.array(pre_orbit['pz_j2000'])
vx_j2000 = np.array(pre_orbit['vx_j2000'])
vy_j2000 = np.array(pre_orbit['vy_j2000'])
vz_j2000 = np.array(pre_orbit['vz_j2000'])
abr = []
elevation = []
for i in range(int(len(px_j2000))):
    tim = Time('{}-{}-{} {}:{}:{}'.format(int(year[i]), int(month[i]), int(day[i]), int(hour[i]), int(minute[i]), int(second[i])))
    pxyz = [px_j2000[i]*1e3,py_j2000[i]*1e3,pz_j2000[i]*1e3] # meters
    vxyz = [vx_j2000[i]*1e3,vy_j2000[i]*1e3,vz_j2000[i]*1e3] # meters per second

    cartdiff = coord.CartesianDifferential(vxyz, unit='km/s')
    jd1,jd2 = erfa.cal2jd(int(year[i]), int(month[i]), int(day[i]))
    #ERA = erfa.era00(jd1,jd2+0.7)
    #print(ERA)
    cartrep = coord.CartesianRepresentation(pxyz, unit=u.km, differentials=cartdiff)
    gcrs = coord.ITRS(cartrep, obstime=tim)
    itrs = gcrs.transform_to(coord.GCRS(obstime=tim))
    #itrs1 = np.dot()
    p_itrs = (itrs.cartesian.xyz)
    #v_itrs = (np.array(itrs.cartesian.differentials['s']/'km/s'))
    #print(p_itrs/u.km-itrs1)    

    azi = cal_azi(p_itrs/u.km)
    ele = cal_ele(p_itrs/u.km)
    
    if ele>20:
        ab = aberration(vxyz, (90-ele)*DD2R, azi*DD2R)
        abr.append(length(ab))
        elevation.append(ele)

        




    #print(gcrs.cartesian.xyz, itrs.cartesian.xyz)
    #print(itrs.cartesian.differentials)
    '''

    GC2IT = GCRS2ITRS(year[i], month[i], day[i], hour[i], minute[i], second[i])
    pxyz_itrs = np.dot(GC2IT,np.array([]))
    print(pxyz_itrs)
    '''
from matplotlib import pyplot as plt
plt.plot(elevation,abr,'.')
plt.xlabel('Elevation [degrees]',fontsize=30)
plt.ylabel('Aberration [arcseconds]',fontsize=30)
plt.tick_params(labelsize = 20)
plt.show()
